﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    struct GameScore
    {
        private int _playerScore;

        private int _houseScore;

        public GameScore(int playerScore, int houseScore)
        {
            _playerScore = playerScore;
            _houseScore = houseScore;
        }

    }

    class CardGame
    {
        private CardDeck _deck;

        private GameScore _score;

        private Card _playerCard;

        private Card _houseCard;

        //TODO: Define the constructor method for the CardGame class
        public CardGame()
        {
            //Initialize ALL field variables
            _deck = new CardDeck();
            _score = new GameScore();
            _playerCard = null;
            _houseCard = null;
        }

        public void Play()
        {
            sbyte roundResult = PlayRound();
        }

        /// <summary>
        /// Plays a round in the game
        /// </summary>
        /// <returns>
        /// +1: the player won the round
        /// -1: the house won the round
        /// 0: the round was a draw
        /// </returns>
        public sbyte PlayRound()
        {
            string exchangeInput;

            byte playerCardRank;

            byte houseCardRank;

            return 0;
        }
    }
}
